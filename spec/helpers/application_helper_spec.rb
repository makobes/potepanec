require 'rails_helper'

RSpec.describe ApplicationHelper, :type => :helper do
  describe "full_title" do
    subject { full_title(page_title) }

    context "when page_title is Ruby Tote Bag" do
      let(:page_title) { "Ruby Tote Bag" }

      it { is_expected.to eq "Ruby Tote Bag | Potepan Store" }
    end

    context "when page_title is empty" do
      let(:page_title) { " " }

      it { is_expected.to eq 'Potepan Store' }
    end

    context "when page_title is nil" do
      let(:page_title) { nil }

      it { is_expected.to eq 'Potepan Store' }
    end
  end

  describe "taxons_tree" do
    context "when current taxon is Bags" do
      let!(:root_taxon)    { create :taxon, id: 1 }
      let!(:current_taxon) { create :taxon, name: "Bags", id: 2, parent_id: 1 }
      let!(:child_taxon)   { create :taxon, name: "child", id: 3, parent_id: 2 }
      let!(:other_root_taxon) { create :taxon, name: "other_root", id: 4 }
      let!(:other_taxon)   { create :taxon, name: "other", id: 5, parent_id: 4 }
      let!(:products)      { create :product, taxons: [current_taxon] }

      it "has link to Bags" do
        expect(helper.taxons_tree(root_taxon, current_taxon)).to include("Bags")
        expect(helper.taxons_tree(root_taxon, current_taxon)).to include(potepan_category_path(current_taxon.id))
      end

      it "can count product" do
        expect(helper.taxons_tree(root_taxon, current_taxon)).to include("(1)")
      end

      it "has link to child_taxon" do
        expect(helper.taxons_tree(root_taxon, current_taxon)).to include("child")
        expect(helper.taxons_tree(root_taxon, current_taxon)).to include(potepan_category_path(child_taxon.id))
      end

      it "does not have other taxon" do
        expect(helper.taxons_tree(root_taxon, current_taxon)).not_to include("other")
      end
    end
  end
end
