require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'Get #show' do
    let(:product) { create :product }

    before do
      get :show, params: { id: product.id }
    end

    it 'returns http success' do
      expect(response).to have_http_status "200"
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end

    it 'renders the :show template' do
      expect(response).to render_template :show
    end
  end
end
