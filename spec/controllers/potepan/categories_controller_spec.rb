require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let(:taxonomy) { create :taxonomy }
    let(:taxon) { create :taxon, taxonomy: taxonomy }
    let!(:product) { create :product, taxons: [taxon] }

    before do
      get :show, params: { id: taxon.id }
    end

    it "returns http success" do
      expect(response).to have_http_status "200"
    end

    it "assigns @taxon" do
      expect(assigns(:taxon)).to eq taxon
    end

    it "assigns @taxonomies" do
      expect(assigns(:taxonomies)).to contain_exactly taxonomy
    end

    it "assigns @product" do
      expect(assigns(:products)).to contain_exactly product
    end

    it "renders the :show template" do
      expect(response).to render_template :show
    end
  end
end
