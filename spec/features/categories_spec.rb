require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  given(:taxonomy)   { create :taxonomy }
  given(:taxon_1)    { create(:taxon,    name: 'Bags', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  given(:taxon_2)    { create(:taxon,    name: 'Mugs', taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  given!(:product_1) { create(:product,  name: 'TOTE', price: 15.99, taxons: [taxon_1]) }
  given!(:product_2) { create(:product,  name: 'RAILS MUG', price: 13.99, taxons: [taxon_2]) }

  feature 'categories page' do
    background do
      visit potepan_category_path(taxon_1.id)
    end

    scenario 'categories page has title, product name and price' do
      expect(page).to have_title 'Bags'

      within '.productBox' do
        expect(page).to have_content product_1.name
        expect(page).to have_content product_1.display_price
      end
    end

    scenario 'categories page can change category' do
      within '.side-nav' do
        expect(page).to have_content taxonomy.name
        click_link taxon_2.name
      end

      expect(page).to have_title 'Mugs'

      within '.productBox' do
        expect(page).to have_content product_2.name
        expect(page).to have_content product_2.display_price
        expect(page).not_to have_content product_1.name
      end
    end

    scenario 'link to product page' do
      click_on product_1.name
      expect(current_path).to eq potepan_product_path(product_1.id)
    end
  end
end
