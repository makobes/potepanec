require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  let(:taxon_1)        { create(:taxon,    name: 'Bags', id: 1) }
  let!(:product_1)     { create(:product,  name: 'TOTE', price: 500.50, taxons: [taxon_1]) }
  let!(:other_product) { create(:product,  name: 'other_product', price: 300.30, id: 2) }

  feature 'products page' do
    background do
      visit potepan_product_path(product_1.id)
    end

    scenario 'products page responce' do
      expect(page).to have_title 'TOTE'
      within '.media-body' do
        expect(page).to have_content 'TOTE'
        expect(page).to have_content '500.50'
      end
    end

    scenario 'link to home' do
      within('ul.nav') do
        expect(page).to have_link 'Home', href: '/potepan'
      end
      within('ol.breadcrumb') do
        expect(page).to have_link 'Home', href: '/potepan'
      end
    end

    scenario 'when @product.taxons is present' do
      expect(page).to have_link '一覧ページへ戻る'
    end

    scenario 'when @product.taxons is not present' do
      visit potepan_product_path(other_product.id)
      expect(page).not_to have_link '一覧ページへ戻る'
    end
  end
end
