module ApplicationHelper
  def full_title(page_title)
    base_title = "Potepan Store"
    if page_title.blank?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def taxons_tree(root_taxon, current_taxon, max_level = 1)
    content_tag :ul do
      taxons = root_taxon.children.map do |taxon|
        content_tag :li do
          link_to("#{taxon.name}(#{taxon.products.size})", potepan_category_path(taxon.id)) +
          taxons_tree(taxon, current_taxon, max_level - 1)
        end
      end
      safe_join(taxons, "\n")
    end
  end
end
